<?php

namespace App\Http\ViewComposers;

// use App\Models\Company;

class CompanyViewComposer
{
    public function compose($view)
    {
        $hostname = app(\Hyn\Tenancy\Environment::class)->hostname();
        if ($hostname) {
            $view->vc_company = \App\Models\Tenant\Company::active();
        } else {
            $view->vc_company = null;
        }

    }
}
