<?php
namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\ClientCollection;
use App\Models\Client;
use App\Models\Tenant\User;

//Multi Tenancy
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Repositories\HostnameRepository;
use Hyn\Tenancy\Repositories\WebsiteRepository;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function columns()
    {
        return [
            'name' => 'Nombre',
        ];
    }

    public function records(Request $request)
    {
        $records = Client::where($request->column, 'like', "%{$request->value}%")
            ->orderBy('name');
        return new ClientCollection($records->paginate(env('ITEMS_PER_PAGE', 5)));
    }

    public function index()
    {
        return view('clients.index');
    }

    public function store(UserRequest $request)
    {
        $fqdn = $request->input('subdominio') . "." . env('APP_DOMAIN');
        error_log($fqdn);
        Client::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'hostname' => $fqdn,
        ]);
        $website = new Website;
        $website->uuid = bin2hex(random_bytes(10));
        app(WebsiteRepository::class)->create($website);
        $hostanme = new Hostname;
        $hostanme->fqdn = $fqdn;
        $hostanme = app(HostnameRepository::class)->create($hostanme);
        app(HostnameRepository::class)->attach($hostanme, $website);

        $environment = app(\Hyn\Tenancy\Environment::class);
        $hostname = Hostname::find($hostanme->id);
        $environment->hostname($hostname);
        $environment->tenant($website);
        $environment->tenant();
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => 'admin',
        ]);

        return [
            'success' => true,
            'message' => 'Cliente registrado',
        ];
        // error_log($request->name);
    }

}
