<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class CatalogController extends Controller
{
    public function index()
    {
        return view('tenant.catalogs.index');
    }
}
