<?php

namespace App\Models\Tenant\Catalogs;

class District extends ModelCatalog
{
    public $incrementing = false;
    public $timestamps = false;

    public static function idByDescription($description, $province_id)
    {
        $district = District::where('description', $description)
            ->where('province_id', $province_id)
            ->first();
        if ($district) {
            return $district->id;
        }
        return '150101';
    }
}
