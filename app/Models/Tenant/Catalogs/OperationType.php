<?php

namespace App\Models\Tenant\Catalogs;

class OperationType extends ModelCatalog
{
    protected $table = "cat_operation_types";
    public $incrementing = false;
}
