<?php

namespace App\Models\Tenant\Catalogs;

class Province extends ModelCatalog
{
    public $incrementing = false;
    public $timestamps = false;

    public static function idByDescription($description)
    {
        $province = Province::where('description', $description)->first();
        if ($province) {
            return $province->id;
        }
        return '1501';
    }
}
