<?php

namespace App\Models\Tenant\Catalogs;

class PerceptionType extends ModelCatalog
{
    protected $table = "cat_perception_types";
    public $incrementing = false;
}
