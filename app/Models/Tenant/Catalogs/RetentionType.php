<?php

namespace App\Models\Tenant\Catalogs;

class RetentionType extends ModelCatalog
{
    protected $table = "cat_retention_types";
    public $incrementing = false;
}
