<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class SummaryDocument extends Model
{
    use UsesTenantConnection;
    protected $with = ['document'];
    public $timestamps = false;

    protected $fillable = [
        'summary_id',
        'document_id',
    ];

    public function document()
    {
        return $this->belongsTo(Document::class);
    }
}
