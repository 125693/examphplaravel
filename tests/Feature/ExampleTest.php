<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function redirect_if_no_logged_id()
    {
        $response = $this->get('/users')->assertRedirect('/login');

        $response->assertStatus(200);
    }
}
