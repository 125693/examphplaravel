<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class ClientModuleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function create_client()
    {
        $user = factory(User::class)->make(['password' => bcrypt('test')]);
        $this->actingAs($user);
        $response = $this->get('/clients')->assertOk();
        $response = $this->post('/clients', [
            'name' => 'TestName',
            'email' => 'TestName@gmail.com',
            'password' => bcrypt('TestName'),
            'subdominio' => 'Test',
        ]);
    }

}
