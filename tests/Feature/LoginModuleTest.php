<?php

namespace Tests\Feature;

use App\Models\User;
// use Illuminate\Database\Eloquent\factory;
use Tests\TestCase;

class LoginModuleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function redirect_if_no_logged_in()
    {
        $response = $this->get('/clients')->assertRedirect('/login');
    }
    /** @test */
    public function show_if_logged()
    {
        $user = factory(User::class)->make(['password' => bcrypt('test')]);
        $this->actingAs($user);
        $response = $this->get('/clients')->assertOk();
    }

}
